<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/csshake.min.css">
        <link rel="stylesheet" href="css/jquery.fullpage.min.css" />

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
<body>

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
	<div class="loader">
	  <div class="dot"></div>
	  <div class="dot"></div>
	  <div class="dot"></div>
	</div>
	
	<div id="site">
    <ul id="menu">
        <a href="#p1" class="active-menu"><li data-menuanchor="p1">დემო</li></a>
        <a href="#p2"><li data-menuanchor="p2">ფუნქციები</li></a>
        <a href="#p3"><li data-menuanchor="p3">F.A.Q.</li></a>
        <a href="#p4"><li data-menuanchor="p4">ფასი</li></a>
        <a href="#p5"><li data-menuanchor="p5">კონტაქტი</li></a>
    </ul>

    <div id="fullpage">
        <div class="section" data-anchor="p1">
            <!-- particles.js container -->
            <div id="particles-js">
                <!-- TEXT -->
                <!-- <h1 class="shake-freeze" style="color: #fff; width: 60%; line-height: 90px; position: absolute; top: 14%; left: 1%; font-size: 80px; text-align: right;">გსურთ რეიტინგის გაზრდა?</h1> -->

<!--
                  <div class="text shake-freeze">
                    <div class="row">
                        <h2>გსურთ რეიტინგის გაზრდა?</h2>
                    </div>
                  </div>
-->
				<div class="words">
					<div class="right">
							<h4 class="randoms"></h4>
							<h1 class="randoms"></h1>
							<h4 class="randoms"></h4>
					</div>
					<div class="left">
							<h4 class="randoms"></h4>
							<h1 class="randoms"></h1>
							<h4 class="randoms"></h4>
					</div>
				</div>
                <!-- Loader 7 -->
                <svg class="" style="position: absolute; z-index: 99; width: 20%; left: 16%; top: 40%;" version="1.1" id="L7" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                 <path fill="#3498bd" d="M31.6,3.5C5.9,13.6-6.6,42.7,3.5,68.4c10.1,25.7,39.2,38.3,64.9,28.1l-3.1-7.9c-21.3,8.4-45.4-2-53.8-23.3
                  c-8.4-21.3,2-45.4,23.3-53.8L31.6,3.5z">
                      <animateTransform 
                         attributeName="transform" 
                         attributeType="XML" 
                         type="rotate"
                         dur="10s" 
                         from="0 50 50"
                         to="360 50 50" 
                         repeatCount="indefinite" />
                  </path>
                 <path fill="#ff4348" d="M42.3,39.6c5.7-4.3,13.9-3.1,18.1,2.7c4.3,5.7,3.1,13.9-2.7,18.1l4.1,5.5c8.8-6.5,10.6-19,4.1-27.7
                  c-6.5-8.8-19-10.6-27.7-4.1L42.3,39.6z">
                      <animateTransform 
                         attributeName="transform" 
                         attributeType="XML" 
                         type="rotate"
                         dur="10s" 
                         from="0 50 50"
                         to="-360 50 50" 
                         repeatCount="indefinite" />
                  </path>
                 <path fill="#fff" d="M82,35.7C74.1,18,53.4,10.1,35.7,18S10.1,46.6,18,64.3l7.6-3.4c-6-13.5,0-29.3,13.5-35.3s29.3,0,35.3,13.5
                  L82,35.7z">
                      <animateTransform 
                         attributeName="transform" 
                         attributeType="XML" 
                         type="rotate"
                         dur="10s" 
                         from="0 50 50"
                         to="360 50 50" 
                         repeatCount="indefinite" />
                  </path>
                </svg>

                <!-- Loader 8 -->
                <!-- <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
                <rect fill="#fff" width="3" height="100" transform="translate(0) rotate(180 3 50)">
                  <animate
                      attributeName="height"
                      attributeType="XML"
                      dur="1s"
                      values="30; 100; 30"
                      repeatCount="indefinite"/>
                </rect>
                <rect x="17" fill="#fff" width="3" height="100" transform="translate(0) rotate(180 20 50)">
                  <animate
                      attributeName="height"
                      attributeType="XML"
                      dur="1s"
                      values="30; 100; 30"
                      repeatCount="indefinite"
                      begin="0.1s"/>
                </rect>
                <rect x="40" fill="#fff" width="3" height="100" transform="translate(0) rotate(180 40 50)">
                  <animate
                      attributeName="height"
                      attributeType="XML"
                      dur="1s"
                      values="30; 100; 30"
                      repeatCount="indefinite"
                      begin="0.3s"/>
                </rect>
                <rect x="60" fill="#fff" width="3" height="100" transform="translate(0) rotate(180 58 50)">
                  <animate
                      attributeName="height"
                      attributeType="XML"
                      dur="1s"
                      values="30; 100; 30"
                      repeatCount="indefinite"
                      begin="0.5s"/>
                </rect>
                <rect x="80" fill="#fff" width="3" height="100" transform="translate(0) rotate(180 76 50)">
                  <animate
                      attributeName="height"
                      attributeType="XML"
                      dur="1s"
                      values="30; 100; 30"
                      repeatCount="indefinite"
                      begin="0.1s"/>
                </rect>
                </svg> -->

                <div class="demo-button" style=" width: 300px; height: 70px; cursor: pointer; background: #fb151b; color: #FFF; font-size: 28px; line-height: 70px; position: absolute; bottom: 12%; left: 14%; z-index: 99;" onclick="location = '/login'"><img src="img/lock.svg"/> შესვლა</div>

                <!-- Arrows -->
                <img class="shake" src="./img/arrows.svg" style="width: 45%; position: absolute; z-index: 99; top: 10%; right: 10%;">
            </div>

        </div>
        <div class="section" data-anchor="p2">
            <h1>ფუნქციები</h1>
            <div class="charts-container cf">
              <div class="chart" id="graph-1-container">
                <h2 class="title">Hours worked</h2>
                <div class="chart-svg">

                  <svg class="chart-line" id="chart-1" viewBox="0 0 80 40">
                    <defs>
                        <clipPath id="clip" x="0" y="0" width="80" height="40" >
                            <rect id="clip-rect" x="-80" y="0" width="77" height="38.7"/>
                        </clipPath>
                        <linearGradient id="gradient-1">
                            <stop offset="0" stop-color="#00d5bd" />
                            <stop offset="100" stop-color="#24c1ed" />
                        </linearGradient>
                        <linearGradient id="gradient-2">
                            <stop offset="0" stop-color="#954ce9" />
                            <stop offset="0.3" stop-color="#954ce9" />
                            <stop offset="0.6" stop-color="#24c1ed" />
                            <stop offset="1" stop-color="#24c1ed" />
                        </linearGradient>
                        <linearGradient id="gradient-3" x1="0%" y1="0%" x2="0%" y2="100%">>
                            <stop offset="0" stop-color="rgba(0, 213, 189, 1)" stop-opacity="0.07"/>
                            <stop offset="0.5" stop-color="rgba(0, 213, 189, 1)" stop-opacity="0.13"/>
                            <stop offset="1" stop-color="rgba(0, 213, 189, 1)" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="gradient-4" x1="0%" y1="0%" x2="0%" y2="100%">>
                            <stop offset="0" stop-color="rgba(149, 76, 233, 1)" stop-opacity="0.07"/>
                            <stop offset="0.5" stop-color="rgba(149, 76, 233, 1)" stop-opacity="0.13"/>
                            <stop offset="1" stop-color="rgba(149, 76, 233, 1)" stop-opacity="0"/>
                        </linearGradient>
                    </defs>
                  </svg>

                  <h3 class="valueX">time</h3>
                </div>
                
                <div class="chart-values">
                    <p class="h-value">1689h</p>
                    <p class="percentage-value"></p>
                    <p class="total-gain"></p>
                </div>
                <div class="triangle green"></div>
              </div>

              <div class="chart" id="graph-2-container">
                <h2 class="title">Hours worked</h2>
                <div class="chart-svg">
                  <svg class="chart-line" id="chart-2" viewBox="0 0 80 40">
                  </svg>
                  <h3 class="valueX">time</h3>
                </div>
                <div class="chart-values">
                  <p class="h-value">322h</p>
                  <p class="percentage-value"></p>
                  <p class="total-gain"></p>
                </div>
                <div class="triangle red"></div>
              </div>
              <div class="chart circle" id="circle-1">
                <h2 class="title">IBApps Website</h2>
                <div class="chart-svg align-center">
                  <h2 class="circle-percentage"></h2>
                  <svg class="chart-circle" id="chart-3" width="50%" viewBox="0 0 100 100">
                    <path class="underlay" d="M5,50 A45,45,0 1 1 95,50 A45,45,0 1 1 5,50"/>
                  </svg>
                </div>
                <div class="triangle green"></div>
              </div>
              <div class="chart circle" id="circle-2">
                <h2 class="title">IBApps Website</h2>
                <div class="chart-svg align-center">
                  <h2 class="circle-percentage"></h2>
                  <svg class="chart-circle" id="chart-4" width="50%" viewBox="0 0 100 100">
                    <path class="underlay" d="M5,50 A45,45,0 1 1 95,50 A45,45,0 1 1 5,50"/>
                  </svg>
                </div>
                <div class="triangle red"></div>
              </div>
            </div>
        </div>
        <div class="section" data-anchor="p3">
            <h1>F.A.Q.</h1>
            <div class="faq">
                <dl>
                  <dt>რას საქმიანობს ჩვენი კომპანია ?</dt>
                  <dd>
                  	ჩვენ საქმიანობაში შედის ინტერნეტ მარკეტინგი. ამ ეტაპზე გვაქვს ორი მიმართულება:
                  		<p>1. კონტენტის პრომოუშენი (მედია კომპანიებისთვის: ინტერნეტ ჟურნალ-გაზეთები, ტელევიზია, გასართობი ინტერნეტ პროტალები და სხა)</p>
                  		<p>2. Facebook თამაშები - (ნებისმიერი ტიპის კომპანიისთვის).</p>
                  </dd>
                  
                  <dt>რატომ facebook თამაშები ?</dt>
                  <dd>Facebook თამაშები - მიეკუთვნება ვირუსული მარკეტინგის მიმართულებას. ვირუსული მარკეტინგი არის მარკეტინგისა და რეკლამის ორიგინალური იდეები და ხერხები, რომლებიც გვეხმარება საქმიანობის ცნობადობის ამაღლებასა და გაყიდვების ზრდაში. ვირუსული მარკეტინგი მისი ორიგინალურობით და არასტანდარტული მიდგომებით იპყრობს პოტენციური მყიდველის ყურადღებას და გავლენას ახდენს მასზე, რაც პროდუქტისა თუ ბრენდის რეალიზაციის დონის ამაღლების წინაპირობაა.
                  </dd>
                  <dt>რას წარმოადგენს Phalcon ?</dt>
                  <dd>Phalcon არის მარკეტინგული ხელსაწყო რომლის გამოყენების შემთხვევაში შესაძლებელია ტრაფიკის (ვიზიტორების) მომატება თქვენს ვებ საიტზე.</dd>
                  <dt>რა პრინციპით მუშაობს ?</dt>
                  <dd>ბევრი არაფერი შეგვიძლია ვთქვათ გარდა იმისა რომ ჩვენ გვყავს უნიკალური უნარების მქონე მარკეტინგის სპეციალისტები და ინოვაციური პროდუქტი.</dd>
                </dl>
            </div>
        </div>
        <div class="section" data-anchor="p4">
            <h1>Phalcon ფასი</h1>
            <div class="whole shake-little">
                <div class="type">
                    <p>1000 ვიზიტორი</p>
                    </div>
                <div class="plan">

                    <div class="header">
                        <span> </span>499<sup>ლ</sup>
                        <p class="month">თვეში</p>
                    </div>
                    <div class="content">
                        <ul>
                            <li>1 ადმინისტრატორი</li>
                            <li>100 დაბუსტვა</li>
                            <li>1 Domain Name</li>
                            <li><img src="./img/cross-2.png" width="28" style="margin-right: 10px;">Scheduler</li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="whole shake">
                <div class="type standard">
                <p>1000-3000 ვიზ.</p>
                </div>
            <div class="plan">

                <div class="header">
                    <span> </span>899<sup>ლ</sup>
                    <p class="month">თვეში</p>
                </div>
                <div class="content">
                    <ul>
                        <li>5 ადმინისტრატორი</li>
                        <li>1000 დაბუსტვა</li>
                        <li>1 Domain Name</li>
                        <li><img src="./img/mark-1.png" width="26" style="margin-right: 10px;">Scheduler</li>
                    </ul>
                </div>
            </div>
            </div>

            <div class="whole shake-hard">
                <div class="type ultimate">
                <p>3000-5000+ ვიზ.</p>
                </div>
                <div class="plan">

                    <div class="header">
                        <span></span>1499<sup>ლ</sup>
                        <p class="month">თვეში</p>
                    </div>
                    <div class="content">
                        <ul>
                            <li>10 ადმინისტრატორი</li>
                            <li>∞ განუსაზღვრელი</li>
                            <li>1 Domain Name</li>
                            <li><img src="./img/mark-1.png" width="26" style="margin-right: 10px;">Scheduler</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="section" data-anchor="p5">
            <h1></h1>
            <div id='browser'>
              <div id='browser-bar'>
                <div class='circles'></div>
                <div class='circles'></div>
                <div class='circles'></div>
                <p>დაგვიკავშირდით</p>
                <span class='arrow entypo-resize-full'></span>
              </div>
              <div id='content'>
                <div id='left'>
                  <div id='map'>
                    <p>ევროპისა და აზიის გასაყარზე</p>
                    <div class='map-locator'>
                      <div class='tooltip'>
                        <ul>
                          <li>
                            <span class='entypo-location'></span>
                            <span class='selectedLocation'>საქართველო, თბილისი</span>
                          </li>
                          <li>
                            <span class='entypo-mail'></span>
                            <a href='#'>info@phalcon.me</a>
                          </li>
                          <li>
                            <span class='entypo-phone'></span>
                            (+995) 598 220 108
                          </li>
                        </ul>
                      </div>
                    </div>
<!--                     <div class='zoom'></div> -->
                  </div>
                  <!-- <ul id='location-bar'>
                    <li>
                      <a class='location' data-location='Israel' href='http://f.cl.ly/items/452R3S1440221Z3m372j/israel.png'>Israel</a>
                    </li>
                    <li>
                      <a class='location' data-location='USA' href='http://f.cl.ly/items/0n0o01382l0p1u271D43/usa.png'>USA</a>
                    </li>
                    <li>
                      <a class='location' data-location='The Netherlands' href='http://f.cl.ly/items/0g0l283X3h1T3H270V0A/netherlands.png'>The Netherlands</a>
                    </li>
                    <li>
                      <a class='location' data-location='Singapore' href='http://f.cl.ly/items/1i2Y262H020X141R3h0k/singapore.png'>Singapore</a>
                    </li>
                  </ul> -->
                </div>
                <div id='right'><!-- 
                  <p>Connect</p>
                  <div id='social'>
                    <a class='social'>
                      <span class='entypo-facebook'></span>
                    </a>
                    <a class='social'>
                      <span class='entypo-twitter'></span>
                    </a>
                    <a class='social'>
                      <span class='entypo-linkedin'></span>
                    </a>
                    <a class='social'>
                      <span class='entypo-gplus'></span>
                    </a>
                    <a class='social'>
                      <span class='entypo-instagrem'></span>
                    </a>
                  </div> -->
                  <form>
                    <p>შეგვეხმიანეთ</p>
                    <input placeholder='ელ-ფოსტა' type='email'>
                    <input placeholder='თემა' type='text'>
                    <textarea placeholder='შეტყობინება' rows='4'></textarea>
                    <input placeholder='გაგზავნა' value='გაგზავნა' type='button'>
                  </form>
                  <p>© 2015</p>
<!--
                  <p class='other entypo-mail'>
                    <a href='#'>givi.kuchukhidze@mail.com</a>
                  </p>
                  <p class='other entypo-phone'>(+995) 598 220 108</p>
-->
                </div>
              </div>
            </div>
        </div>
    </div>
   </div> 
    
    <!-- <div class="login_c">
      <div class="type login"><p>MEMBER LOGIN</p></div>
      
<form action="#">
  <div class="top">
    <label for="name"></label>
    <input type="text" name="name" class="nodisplay" id="name" placeholder="Username" required/>
  </div>
    <div>
    <label for="name"></label>
    <input type="text" name="name" id="name" class="nodisplay" placeholder="Password" required />
  </div>

  
  <div>
    <input type="submit" value="Submit" />
  </div>
</form>  
  
</div> -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="./js/vendor/snap.svg-min.js"></script>
    <script src="./js/vendor/jquery.fullpage.min.js"></script>
    <script src="./js/vendor/particles.min.js"></script>
    <script src="./js/plugins.js"></script>
    <script src="./js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','UA-XXXXX-X','auto');ga('send','pageview');
    </script>
</body>
</html>