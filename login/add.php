<?php require_once "inc/inc.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Phalcon Admin :: Statistics</title>
	
	<link rel="icon" sizes="192x192" href="img/touch-icon.png" /> 
	<link rel="apple-touch-icon" href="img/touch-icon-iphone.png" /> 
	<link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
	<link rel="apple-touch-icon" sizes="120x120" href="img/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
	
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
	<link rel="stylesheet" type="text/css" href="css/rome.css">
	
</head>
<body class="bg3">

	<div id="loading">
		<div class="loader loader-light loader-large"></div>
	</div>
	
	<?php require_once "inc/header.php"; ?>

	<div class="wrapper">

		<?php require_once "inc/sidebar.php"; ?>

		<section class="content">
			<header class="main-header">
				<div class="main-header__nav">
					<h1 class="main-header__title">
						<i class="pe-7f-note"></i>
						<span>Add - New</span>
					</h1>
					<ul class="main-header__breadcrumb">
						<li><a href="#" onclick="return false;">Home</a></li>
						<li class="active"><a href="#" onclick="return false;">Add - New</a></li>
					</ul>
				</div>
				
			</header> <!-- /main-header -->

				<div class="row">

					<div class="alert alert-fixed alert-success alert-dismissible mess-success" role="alert">
						<button type="button" class="close" data-dismiss="alert">
							<span aria-hidden="true"><i class="pe-7s-close"></i></span>
						</button>
						<div class="alert__icon pull-left">
							<i class="pe-7s-check"></i>
						</div>
						<p class="alert__text"> Success.. News has been added to Data Base...</p>
					</div>

					<div class="alert alert-fixed alert-warning alert-dismissible mess-error" role="alert">
						<button type="button" class="close" data-dismiss="alert">
							<span aria-hidden="true"><i class="pe-7s-close"></i></span>
						</button>
						<div class="alert__icon pull-left">
							<i class="pe-7s-attention"></i>
						</div>
						<p class="alert__text"> Error!! Please fill everything correctly...</p>
					</div>

					<div class="col-md-5 add-new">
						<article class="widget widget__form">
							<header class="widget__header">
								<div class="widget__title">
									<i class="pe-7f-date"></i><h3>Schedule New Post</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
									<a href="#"><i class="pe-7s-close"></i></a>
								</div>
							</header>

							<div class="widget__content">
								<label for="input-1" class="stacked-label"><i class="pe-7f-link"></i></label><input type="text" class="stacked-input share-link" id="input-1" placeholder="Link">
								<label for="input-2" class="stacked-label label-descr"><i class="pe-7f-note"></i></label><textarea disabled="disabled" class="textarea description" id="input-2" placeholder="Description"></textarea>
								<label for="input-3" class="stacked-label label-datetime"><i class="pe-7f-timer"></i></label><input disabled="disabled" class='input stacked-input date-time' value='2014-12-15 21:00' type="text" id="input" placeholder="Date-Time">
								<label class="full-label">
								</label><button disabled="disabled" id="submitbtn" class="submit-btn"><i class="pe-7f-date"></i> &nbsp; &nbsp;Schedule</button>
						</div>
					</div>

					<article class="widget col-md-7 widget-fb">
						<header class="widget__header">
							<div class="widget__title">
								<!-- <i class="pe-7*-icon"></i><h3>Widget container</h3> -->
							</div>
							<div class="widget__config">
								<a href="#"><i class="pe-7*-icon"></i></a>
								<a href="#"><i class="pe-7*-icon"></i></a>
							</div>
						</header>

						<div class="widget__content filled">
							<div class="loader loader-light loader-large siteloading"></div>
							<div class="fb-share">
								<div class="comment">Culpa dolorum velit eos, commodi natus cumque placeat neque fugit sit itaque numquam laboriosam iusto sequi, nulla impedit illo, maxime eaque incidunt.</div>
								<div class="fb-card">
									<div class="thumb">
										<img src="http://lorempixel.com/550/300/sports" />
									</div>
									<div class="info">
										<div class="title">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
										</div>
										<div class="descr">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa dolorum velit eos, commodi natus cumque placeat neque fugit sit itaque numquam laboriosam iusto sequi, nulla impedit illo, maxime eaque incidunt.
										</div>
										<div class="site">SITE.COM</div>
									</div>
								</div>
							</div>
						</div>
					</article>
					

				</div> <!-- /row -->

			<footer class="footer-brand">
				<img src="img/logo_trim.png">
				<p>© 2014 Phalcon. All rights reserved</p>
			</footer>


		</section> <!-- /content -->

	</div>

	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/app.js"></script>
	<script type="text/javascript" src="js/rome.min.js"></script>
	<script>
		$(document).ready(function(){
			rome(input, { initialValue: '<?=date("Y-m-d H:i:s")?>' });
			// rome(inputTwo, { initialValue: '2014-12-08 08:36' });

			$('.add-new .description').bind("change paste keyup", function() {
				$('.fb-share .comment').html($(this).val());
			});

			$('.add-new .share-link').bind("change", function() {
				$('.fb-share').css('background', '#fff');
				var url = $(this).val().trim();
				if(isURL(url)){

					// show the loading message.
		    		$('.siteloading').show();
		    		$('.fb-share').css('opacity', '0.21');
					$.ajax({
						type: 'GET', 
						url: 'parse.php?url=' + url,
						dataType: 'html',
						success: function(data) {
							var metas = JSON.parse(data);
							console.log(metas);
							// hide the loading message
							$('.siteloading').hide();
		    				$('.fb-share').css('opacity', '1');
		 					
		 					// Element(s) are now enabled.
		    				$('.add-new .description').prop("disabled", false);
		    				$('.add-new .date-time').prop("disabled", false);
		    				$('.add-new .submit-btn').prop("disabled", false);

		    				// clear data
							$('.fb-share .comment, .fb-share .info .title, .fb-share .info .descr, .fb-share .info .site').html('');
							$('.fb-share .thumb > img').attr('src', '')

		    				// set image
							$('.fb-share .thumb > img').attr('src', metas.image)
							// set title
							$('.fb-share .info .title').html(metas.title)
							// set description
							$('.fb-share .info .descr').html(metas.description)
							// set url
							if(metas.url)
								var site = metas.url.match(/:\/\/(.[^/]+)/)[1];
							$('.fb-share .info .site').html(site)

						}
					});
				}
			});

			// everything ok, submit
			$('#submitbtn').click(function() {
				$('.add-new, .widget-fb').fadeOut("fast");

				var dataSend = {};

				// organize data
				dataSend['LINK'] = $('.share-link').val();
				dataSend['COMMENT'] = $('.description').val();
				dataSend['TITLE'] = $('.fb-share .info .title').html();
				dataSend['IMAGE'] = $('.thumb img').attr("src");
				dataSend['DESCRIPTION'] = $('.fb-share .info .descr').html();
				dataSend['SCHEDULE'] = $('.date-time').val();

				// send data to service
			    $.ajax({
			        url: "exec.php?act=add",
			        type: "post",
			        data: dataSend
			    })
			    .done(function(resp) {
			    	if(resp == 1){
			    		//  success
						window.setTimeout(function(){
							$('.mess-success').fadeIn('slow');
						}, 250);

						//  redirect
						window.setTimeout(function(){
				    		window.location = 'list.php';
						}, 1600);
			    	}
			    	else
			    	{
			    		//  error
						window.setTimeout(function(){
							$('.mess-error').fadeIn('slow');
						}, 250);

						window.setTimeout(function(){
							$('.mess-error').fadeOut('slow');
						}, 3500);
						
						// show form again
						window.setTimeout(function(){
							$('.add-new, .widget-fb').fadeIn('slow');
						}, 900);
			    	}
				    	
				})
				.fail(function() {
					//  error
					window.setTimeout(function(){
						$('.mess-error').fadeIn('slow');
					}, 250);
					
					// show form again
					window.setTimeout(function(){
						$('.add-new, .widget-fb').fadeIn('slow');
					}, 900);
				})
				.always(function() {
				});
					
			});
		})
	</script>
</body>
</html>