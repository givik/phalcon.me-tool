<?php
	require_once "inc/inc.php";
	
	// Moth
	$date = new DateTime();
	$date->modify("-1 month");
	$mothago = $date->format('Y-m-d');
	// Week
	$date = new DateTime();
	$date->modify("-7 day");
	$weekago = $date->format('Y-m-d');
	// Yesterday
	$date = new DateTime();
	$date->modify("-1 day");
	$yesterday = $date->format('Y-m-d');
	// Today
	$date = new DateTime();
	$today = $date->format('Y-m-d');
	
	$monStat = $db->sql("SELECT * FROM `STATISTICS` WHERE DATE(DATETIME) >= '". $mothago ."'")->many();
	$weeStat = $db->sql("SELECT * FROM `STATISTICS` WHERE DATE(DATETIME) >= '". $weekago ."'")->many();
	$yesStat = $db->sql("SELECT * FROM `STATISTICS` WHERE DATE(DATETIME) = '". $yesterday ."'")->many();
	$todStat = $db->sql("SELECT * FROM `STATISTICS` WHERE DATE(DATETIME) = '". $today ."'")->many();
	
/*
	print_r("<pre>");
	print_r($monStat);
	print_r("</pre>");
*/
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Phalcon Admin :: Dashboard</title>
	
	<link rel="icon" sizes="192x192" href="img/touch-icon.png" /> 
	<link rel="apple-touch-icon" href="img/touch-icon-iphone.png" /> 
	<link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
	<link rel="apple-touch-icon" sizes="120x120" href="img/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
	
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
	
</head>
<body class="bg3">
	<div id="loading">
		<div class="loader loader-light loader-large"></div>
	</div>
	
	<?php require_once "inc/header.php"; ?>

	<div class="wrapper">

		<?php require_once "inc/sidebar.php"; ?>
		
		<section class="content">
			<header class="main-header">
				<div class="main-header__nav">
					<h1 class="main-header__title">
						<i class="pe-7f-home"></i>
						<span>Dashboard</span>
					</h1>
					<ul class="main-header__breadcrumb">
						<li><a href="#" onclick="return false;">Home</a></li>
						<li><a href="#" onclick="return false;">Dashboard</a></li>
					</ul>
				</div>
				
				<div class="main-header__date">
					<input type="radio" id="radio_date_1" name="tab-radio" value="today" checked><!--
					--><label class="fixed-width" for="radio_date_1">Today</label><!--
					--><input type="radio" id="radio_date_2" name="tab-radio" value="yesterday"><!--
					--><label class="fixed-width" for="radio_date_2">Yesterday</label><!--
					--><button>
						<i class="pe-7f-date"></i>
						<span><?=date('Y-m-d')?></span>
					</button>
				</div>
			</header> <!-- /main-header -->

			<div data-tab-radio="tab-radio" class="tab-radio-content row" id="today">
					<div class="main-stats__stat col-md-3 col-sm-3">
						<h3 class="main-stats__title">Resume<br> of the day.</h3>
						<p class="main-stats__resume" style="font-size: 13px;">Marketing / Analytics is power - if you have the right tools. Phalcon's suite transforms data into meaningful insights that lead to competitive advantage for your company.</p>
						
					</div> <!-- /col -->
				  
				  <div class="main-stats__stat col-md-3 col-sm-3 col-xs-4">
						<div class="stat-circle">
							<h3>999</h3>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 92 92">
								<circle style="opacity:0.16;fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:10;" cx="46" cy="46" r="45"/>
								<path style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;" d="M84.839,68.718C88.749,62.049,91,54.289,91,46C91,21.147,70.853,1,46,1"/>
							</svg>
						</div> <!-- /stat-circle -->
						<h4 class="main-stats__subtitle">Total<br> visitors<br>
						</h4>
					</div> <!-- /col -->

					<div class="main-stats__stat col-md-3 col-sm-3 col-xs-4">
						<div class="stat-circle">
							<h3>888</h3>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 92 92">
							<circle style="opacity:0.16;fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:10;" cx="46" cy="46" r="45"/>
							<path style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;" d="M6.185,66.968C13.725,81.256,28.721,91,46,91c24.853,0,45-20.147,45-45C91,21.147,70.853,1,46,1"/>
							</svg>
						</div> <!-- /stat-circle -->
						<h4 class="main-stats__subtitle">New<br> users<br>
							<span class="main-stats__resume">+ 65%</span>
						</h4>

					</div> <!-- /col -->

					<div class="main-stats__stat col-md-3 col-sm-3 col-xs-4">
						<div class="stat-circle">
							<h3>999</h3>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 92 92">
								<circle style="opacity:0.16;fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:10;" cx="46" cy="46" r="45"/>
								<path style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;" d="M91,46C91,21.147,70.853,1,46,1"/>
							</svg>
						</div> <!-- /stat-circle -->
						<h4 class="main-stats__subtitle">Old<br> users<br>
							<span class="main-stats__resume">+ 35%</span>
						</h4>
					</div> <!-- /col -->

				</div> <!-- row -->

				<div data-tab-radio="tab-radio" class="tab-radio-content row" id="yesterday">
					<div class="main-stats__stat col-md-3 col-sm-3">
						<h3 class="main-stats__title">Resume<br> of yesterday.</h3>
						<p class="main-stats__resume" style="font-size: 13px;">Marketing / Analytics is power - if you have the right tools. Phalcon's suite transforms data into meaningful insights that lead to competitive advantage for your company.</p>
						
					</div> <!-- /col -->
				  
				  <div class="main-stats__stat col-md-3 col-sm-3 col-xs-4">
						<div class="stat-circle">
							<h3>999</h3>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 92 92">
								<circle style="opacity:0.16;fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:10;" cx="46" cy="46" r="45"/>
								<path style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;" d="M84.839,68.718C88.749,62.049,91,54.289,91,46C91,21.147,70.853,1,46,1"/>
							</svg>
						</div> <!-- /stat-circle -->
						<h4 class="main-stats__subtitle">Total<br> visitors<br>
						</h4>
					</div> <!-- /col -->

					<div class="main-stats__stat col-md-3 col-sm-3 col-xs-4">
						<div class="stat-circle">
							<h3>888</h3>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 92 92">
							<circle style="opacity:0.16;fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:10;" cx="46" cy="46" r="45"/>
							<path style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;" d="M6.185,66.968C13.725,81.256,28.721,91,46,91c24.853,0,45-20.147,45-45C91,21.147,70.853,1,46,1"/>
							</svg>
						</div> <!-- /stat-circle -->
						<h4 class="main-stats__subtitle">New<br> users<br>
							<span class="main-stats__resume">+ 61%</span>
						</h4>

					</div> <!-- /col -->

					<div class="main-stats__stat col-md-3 col-sm-3 col-xs-4">
						<div class="stat-circle">
							<h3>999</h3>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 92 92">
								<circle style="opacity:0.16;fill:none;stroke:#000000;stroke-width:2;stroke-miterlimit:10;" cx="46" cy="46" r="45"/>
								<path style="fill:none;stroke:#FFFFFF;stroke-width:2;stroke-miterlimit:10;" d="M91,46C91,21.147,70.853,1,46,1"/>
							</svg>
						</div> <!-- /stat-circle -->
						<h4 class="main-stats__subtitle">Old<br> users<br>
							<span class="main-stats__resume">+ 39%</span>
						</h4>
					</div> <!-- /col -->

				</div> <!-- row -->


				<div class="row">
					
					<div class="col-md-7">
						<article class="widget">
							<header class="widget__header">
								<div class="widget__title">
									<i class="pe-7f-graph3"></i><h3>Moth Statistics</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
									<a href="#"><i class="pe-7s-close"></i></a>
								</div>
							</header>

							<div class="widget__content filled">
								<p class="graph-number"><span>55,555</span> Visits</p>
								<div id="chartdiv" style="width: 100%; height: 362px;"></div>

							</div>
						</article><!-- /widget -->
					</div>

					<div class="col-md-5">
						<article class="widget">
							<header class="widget__header">
								<div class="widget__title">
									<i class="pe-7f-graph"></i><h3>Month - Devices</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
									<a href="#"><i class="pe-7s-close"></i></a>
								</div>
							</header>

							<div class="widget__content filled">
								<p class="graph-number"><span>2,222</span> Total Visits</p>
								<div class="pie-container">
									<div class="pie-number">
										<h3>&nbsp;<i class="pe-7f-phone"></i></h3>
										<!-- <p>my balance</p> -->
									</div>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 180 179">
										<path fill="#ff3a2f" d="M88,0C48.6,0.9,15.4,27,4,62.9L7.9,64C18.8,29.8,50.4,4.9,88,4V0z"/>
										<path fill="#52d669" d="M113.4,167.6C106,169.8,98,171,89.9,171C44.7,171,8,134.4,8,89.4c0-8,1.2-15.8,3.4-23.1L3.6,64
											C1.3,72,0,80.5,0,89.4C0,138.9,40.2,179,89.9,179c9.1,0,17.8-1.4,26.1-3.9L113.4,167.6z"/>
										<path fill="#1c7dfa" d="M90,0v16c40.8,0,74,33.1,74,73.8c0,32.5-21.2,60.2-50.5,70l5.1,15.2c35.7-11.9,61.4-45.6,61.4-85.2
											C180,40.2,139.7,0,90,0z"/>
									</svg>
								</div>

							</div>

							<div class="widget__content filled">
								<div class="row">
									<div class="col-md-4 col-sm-4 col-xs-4 mini-stats">
										<div class="pie-small blue"></div>
										<p>
											<span>43%</span><br>
											Computers
										</p>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4 mini-stats">
										<div class="pie-small green"></div>
										<p>
											<span>36%</span><br>
											Phones
										</p>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4 mini-stats">
										<div class="pie-small red"></div>
										<p>
											<span>21%</span><br>
											Tablets
										</p>
									</div>
								</div>
							</div>

						</article><!-- /widget -->
					</div>


				</div> <!-- /row -->


				<div class="tabs">
					<input type="radio" id="tab1" name="msgs_tabs" checked>
				</div> <!-- /tabs -->

			<footer class="footer-brand">
				<img src="img/logo_trim.png">
				<p>© 2014 Phalcon. All rights reserved</p>
			</footer>


		</section> <!-- /content -->

	</div>


	 
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/amcharts/amcharts.js"></script>
	<script type="text/javascript" src="js/amcharts/serial.js"></script>
	<script type="text/javascript" src="js/amcharts/pie.js"></script>
	<script type="text/javascript" src="js/chart.js"></script>
</body>
</html>