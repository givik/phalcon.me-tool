<?php
	// access origin *
	header("Access-Control-Allow-Origin: *");

	// url insurance
	if(!isset($_GET['act']) && !$_GET['act'])
		header('Location: index.php');

	// include preferences
	require_once "inc/inc.php";

	switch ($_GET['act']) {
		// GET records from db
		case 'get':

				// select articles
				$data = $db->from('ARTICLES')->where('USERID', $userInfo['ID'])->many();

				if(!empty($data))
					echo json_encode($data, JSON_PRETTY_PRINT);
				else
					echo 0;

			break;

		// ADD records to db
		case 'add':

				// url ensurance
				if ($userInfo['URL'] && strpos($_POST['LINK'], $userInfo['URL']) == false) {
				    die(0);
				}

				// push extra data
				$dataADD = $_POST;
				$dataADD['USERID'] = $userInfo['ID'];
				$dataADD['ADDDATE'] = date('Y-m-d H:i:s');

				// execute
			    $res = $db->from('ARTICLES')->insert($dataADD)->execute();
			    
				if($res)
					echo 1;
				else
					echo 0;

			break;
		
		// EDIT records in db
		case 'edit':

				/*print_r("<pre>");
				print_r($_POST);
				print_r("</pre>");*/

				// execute
			    // $res = $db->from('articles')->insert($dataADD)->execute();
			    
				if($res)
					echo 1;
				else
					echo 0;
			
			break;
		
		// DELETE records in db
		case 'del':

				// delete
			    $res = $db->from('ARTICLES')->where('ID', $_POST['ID'])->delete()->execute();

				if($res)
					echo 1;
				else
					echo 0;

			break;
		
		default:
				header('Location: index.php');
			break;
	}

?>