<?php require_once "inc/inc.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Phalcon Admin :: Grid Layout</title>
	
	<link rel="icon" sizes="192x192" href="img/touch-icon.png" /> 
	<link rel="apple-touch-icon" href="img/touch-icon-iphone.png" /> 
	<link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
	<link rel="apple-touch-icon" sizes="120x120" href="img/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
	
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.min.css">
</head>
<body class="bg3">
	<div id="loading">
		<div class="loader loader-light loader-large"></div>
	</div>
	
	<?php require_once "inc/header.php"; ?>


	<div class="wrapper">

		<?php require_once "inc/sidebar.php"; ?>
		
		<section class="content">
			<header class="main-header">
				<div class="main-header__nav">
					<h1 class="main-header__title">
						<i class="pe-7f-browser"></i>
						<span>Grid Layout</span>
					</h1>
					<ul class="main-header__breadcrumb">
						<li><a href="#" onclick="return false;">Home</a></li>
						<li class="active"><a href="#" onclick="return false;">Grid Layout</a></li>
					</ul>
				</div>
				
			</header> <!-- /main-header -->





				<div class="row">
					
					<div class="col-md-12">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>12 Columns</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam harum, consequuntur voluptates consectetur tempora explicabo distinctio nemo alias. Ipsum magni ut est cumque mollitia, blanditiis libero facere facilis illum quisquam ducimus qui dicta, provident quasi eveniet sapiente pariatur quas asperiores delectus quis iure magnam minus! Iste excepturi at eveniet nemo tenetur deleniti quas blanditiis, sapiente, labore qui quisquam quos pariatur quidem obcaecati animi ducimus illo expedita dolore officiis? Consequatur, atque, unde fugiat magni dolores temporibus error quo praesentium quibusdam a, alias, assumenda delectus? Vel sint tempore deserunt possimus magnam illo fugit neque laborum! In sint modi non totam necessitatibus excepturi, voluptatibus labore quaerat facere natus provident tenetur. Dolorem facilis sequi quibusdam quaerat, minima reiciendis sit iure eos, totam explicabo architecto!</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

				</div> <!-- /row -->


				<div class="row">
					
					<div class="col-md-6">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>6 Columns</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ratione, odit minus quo reiciendis illum asperiores, in deserunt qui, dolor placeat, totam! Corporis veniam atque tempore dolore quos doloremque vero fuga alias, quasi, sed, iste quas ad distinctio! Quasi sint, voluptatibus natus molestiae minus perferendis dolorem qui tempora velit animi, eaque sequi sunt pariatur quod earum. Culpa harum neque dolorem!</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

					<div class="col-md-6">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>6 Columns</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor necessitatibus sed, unde neque ut ipsa inventore eum perferendis, quos tempore accusantium earum libero quidem quis, asperiores soluta labore. Veniam eveniet corporis in iste, at doloremque quia porro animi perspiciatis, consequuntur unde optio neque maxime saepe magnam deleniti similique placeat numquam, maiores quo? Illo suscipit saepe, aut alias exercitationem at quia.</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

				</div> <!-- /row -->


				<div class="row">
					
					<div class="col-md-4">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>4 Columns</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ratione, odit minus quo reiciendis illum asperiores, in deserunt qui, dolor placeat, totam! Corporis veniam atque tempore dolore quos doloremque vero fuga alias, quasi, sed, iste quas ad distinctio! Quasi sint, voluptatibus natus molestiae minus perferendis dolorem qui tempora velit animi, eaque sequi sunt pariatur quod earum. Culpa harum neque dolorem!</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

					<div class="col-md-4">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>4 Columns</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor necessitatibus sed, unde neque ut ipsa inventore eum perferendis, quos tempore accusantium earum libero quidem quis, asperiores soluta labore. Veniam eveniet corporis in iste, at doloremque quia porro animi perspiciatis, consequuntur unde optio neque maxime saepe magnam deleniti similique placeat numquam, maiores quo? Illo suscipit saepe, aut alias exercitationem at quia.</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>


					<div class="col-md-4">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>4 Columns</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor necessitatibus sed, unde neque ut ipsa inventore eum perferendis, quos tempore accusantium earum libero quidem quis, asperiores soluta labore. Veniam eveniet corporis in iste, at doloremque quia porro animi perspiciatis, consequuntur unde optio neque maxime saepe magnam deleniti similique placeat numquam, maiores quo? Illo suscipit saepe, aut alias exercitationem at quia.</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

				</div> <!-- /row -->



				<div class="row">
					
					<div class="col-md-3">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>3 col</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor necessitatibus sed, unde neque ut ipsa inventore eum perferendis, quos tempore accusantium earum libero quidem quis, asperiores soluta labore. Veniam eveniet corporis in iste, at doloremque quia porro animi perspiciatis, consequuntur unde optio neque maxime saepe magnam deleniti similique placeat numquam, maiores quo? Illo suscipit saepe, aut alias exercitationem at quia.</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

					<div class="col-md-3">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>3 col</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor necessitatibus sed, unde neque ut ipsa inventore eum perferendis, quos tempore accusantium earum libero quidem quis, asperiores soluta labore. Veniam eveniet corporis in iste, at doloremque quia porro animi perspiciatis, consequuntur unde optio neque maxime saepe magnam deleniti similique placeat numquam, maiores quo? Illo suscipit saepe, aut alias exercitationem at quia.</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>


					<div class="col-md-3">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>3 col</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor necessitatibus sed, unde neque ut ipsa inventore eum perferendis, quos tempore accusantium earum libero quidem quis, asperiores soluta labore. Veniam eveniet corporis in iste, at doloremque quia porro animi perspiciatis, consequuntur unde optio neque maxime saepe magnam deleniti similique placeat numquam, maiores quo? Illo suscipit saepe, aut alias exercitationem at quia.</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

					<div class="col-md-3">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>3 col</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor necessitatibus sed, unde neque ut ipsa inventore eum perferendis, quos tempore accusantium earum libero quidem quis, asperiores soluta labore. Veniam eveniet corporis in iste, at doloremque quia porro animi perspiciatis, consequuntur unde optio neque maxime saepe magnam deleniti similique placeat numquam, maiores quo? Illo suscipit saepe, aut alias exercitationem at quia.</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

				</div> <!-- /row -->

				<div class="row">
					<div class="col-md-7">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>7 Colums</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor necessitatibus sed, unde neque ut ipsa inventore eum perferendis, quos tempore accusantium earum libero quidem quis, asperiores soluta labore. Veniam eveniet corporis in iste, at doloremque quia porro animi perspiciatis, consequuntur unde optio neque maxime saepe magnam deleniti similique placeat numquam, maiores quo? Illo suscipit saepe, aut alias exercitationem at quia.</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->

					</div> 

					<div class="col-md-5">
						<article class="widget">
							<header class="widget__header one-btn">
								<div class="widget__title">
									<i class="pe-7f-menu pe-rotate-90"></i><h3>5 Colums</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
								</div>
							</header>
							
							<div class="widget__content widget__grid filled pad20">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor necessitatibus sed, unde neque ut ipsa inventore eum perferendis, quos tempore accusantium earum libero quidem quis, asperiores soluta labore. Veniam eveniet corporis in iste, at doloremque quia porro animi perspiciatis.</p>
							</div> <!-- /widget__content -->

						</article><!-- /widget -->

					</div> 

				</div> <!-- /row -->



			<footer class="footer-brand">
				<img src="img/logo_trim.png">
				<p>© 2014 Phalcon. All rights reserved</p>
			</footer>


		</section> <!-- /content -->

	</div>


	
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/amcharts/amcharts.js"></script>
	<script type="text/javascript" src="js/amcharts/serial.js"></script>
	<script type="text/javascript" src="js/amcharts/pie.js"></script>
	<script type="text/javascript" src="js/chart.js"></script>
</body>
</html>