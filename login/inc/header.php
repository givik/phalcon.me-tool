<header class="top-bar">
		
	<ul class="profile"> 
		<li>
			<a href="#" class="btn-circle no-circle">
				<i class="pe-7f-back"></i>
			</a>
		</li>
		<li>
			<a href="#" onclick="return false;" class="btn-circle btn-sm">
				<i class="pe-7f-mail"></i>
				<span class="badge badge--black">0</span>
			</a>
		</li>
		<li>
			<a href="#" onclick="return false;" class="btn-circle btn-sm">
				<i class="pe-7g-sets"></i>
			</a>
		</li>
		<li>
			<a href="#" onclick="return false;" class="btn-circle btn-sm ">
				<i class="pe-7f-power"></i>
			</a>
		</li>
		<li class="mobile-nav">
			<a href="#" onclick="return false;" class="btn-circle btn-sm">
				<i class="pe-7f-menu"></i>
			</a>
		</li>
	</ul>

	<div class="main-search">
		<input type="text" placeholder="Search ..." id="msearch">
		<label for="msearch">
			<i class="pe-7s-search"></i>
		</label>
		<button>
			<i class="pe-7g-arrow-circled pe-rotate-90"></i>
		</button>
	</div>

	<div class="main-brand">
		<div class="main-brand__container">
			<div class="main-logo"><img src="img/logo.png"></div>
			<input type="checkbox" id="s-logo" class="sw" />
			<label class="swtc swtc--dark swtc--header" for="s-logo"></label> 
		</div>
	</div>

</header> <!-- /top-bar -->