<?php

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	
	// define default timezone
	date_default_timezone_set('Asia/Tbilisi');

	require_once __DIR__ . "/sparrow.php";

	// Declare the class instance
	$db = new Sparrow();

	// db configuration
	$mysqli = mysqli_connect('localhost', 'root', 'fkg7h2k', 'phalcondb');

	// set db preferences
	$db->setDb($mysqli);
	$db->sql("SET NAMES 'utf8'")->execute();

	// get user info
	$userInfo = $db->from('USERS')->where('ID', 1)->one();