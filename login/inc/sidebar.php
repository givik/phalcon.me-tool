<?php
	
	$Num = $db->from('ARTICLES')->where(array('PUBLISHED' => '0'))->count();
?>
<aside class="sidebar">
			
	<div class="user-info">
		<a href="dashboard.php">
			<figure class="rounded-image profile__img">
				<img class="media-object" src="img/profile.png" alt="user">
			</figure>
		</a>
		<h2 class="user-info__name">JAKO.FM</h2>
		<h3 class="user-info__role">Admin Manager</h3>
		<ul class="user-info__numbers">
			<li>
				<i class="pe-7s-paper-plane"></i>
				<p>15</p>
				<p></p>
			</li>
			<li>
				<i class="pe-7g-watch"></i>
				<p>1653</p>
				<p></p>
			</li>
			<li>
				<i class="pe-7f-user"></i>
				<p>+1347</p>
				<p></p>
			</li>
		</ul>
	</div> <!-- /user-info -->

	<ul class="main-nav">
		<li class="main-nav--active">
			<a class="main-nav__link" href="dashboard.php">
				<span class="main-nav__icon"><i class="pe-7f-home"></i></span>
				Dashboard
			</a>
		</li>
		<li class="main-nav--collapsible">
			<a class="main-nav__link" href="#" onclick="return false;">
				<span class="main-nav__icon"><i class="pe-7f-monitor"></i></span>
				Boosted News <span class="badge badge--line badge--blue"><?=$Num?></span>
			</a>
			<ul class="main-nav__submenu">
				<li><a href="add.php"><span>+ Add New</span></a></li>
				<li><a href="list.php"><span>List</span></a></li>
			</ul>
		</li>
		<li>
			<a class="main-nav__link" href="stats.php">
				<span class="main-nav__icon"><i class="pe-7f-graph3"></i></span>
				Statistics
			</a>
		</li>
	</ul> <!-- /main-nav -->
	
</aside> <!-- /sidebar -->