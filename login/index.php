<?php require_once "inc/inc.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Phalcon Admin :: Sign in</title>
	
	<link rel="icon" sizes="192x192" href="img/touch-icon.png" /> 
	<link rel="apple-touch-icon" href="img/touch-icon-iphone.png" /> 
	<link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
	<link rel="apple-touch-icon" sizes="120x120" href="img/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
	
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	
	
    <script src="./js/particles.min.js"></script>
	
	<style>
		.widget__login {
		    margin-top: calc(40vh - 63px*4/2);
		}
		.rocket {
			display: none;
		}
		body {
			overflow: hidden;
		}
	</style>
	
</head>
<body class="bg3" id="particles-js">
	<div>
		<div class="alert alert-fixed alert-warning alert-dismissible mess-error" role="alert">
			<button type="button" class="close" data-dismiss="alert">
			</button>
			<div class="alert__icon pull-left">
				<i class="pe-7s-attention"></i>
			</div>
			<p class="alert__text"> Wrong Username or Password!! Please check Credentials..</p>
		</div>
		
	<div class="col-md-4  col-md-offset-4">
		<article class="widget widget__login">
			<div align="center" class="rocket">
			<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGlkPSLQodC70L7QuV8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48Zz48cGF0aCBkPSJNMTI2LDY0YzAsMzMuNS0yNi41LDYwLjctNTkuNyw2MmMwLDAtMS41LTItMi4zLTJjLTAuOCwwLTIuMywyLTIuMywyQzI4LjUsMTI0LjgsMiw5Ny41LDIsNjQgICBDMiwyOS44LDI5LjgsMiw2NCwyUzEyNiwyOS44LDEyNiw2NHoiIGZpbGw9IiMyRjQ3NTAiLz48Y2lyY2xlIGN4PSI5MC40IiBjeT0iMzQuNiIgZmlsbD0iI0ZGRkZGRiIgb3BhY2l0eT0iMC44IiByPSIxIi8+PGNpcmNsZSBjeD0iMTAuNCIgY3k9IjU0LjYiIGZpbGw9IiNGRkZGRkYiIG9wYWNpdHk9IjAuOCIgcj0iMSIvPjxjaXJjbGUgY3g9IjYwLjQiIGN5PSIxMDQuNiIgZmlsbD0iI0ZGRkZGRiIgb3BhY2l0eT0iMC44IiByPSIxIi8+PGcgb3BhY2l0eT0iMC44Ij48Y2lyY2xlIGN4PSIxMTAiIGN5PSI1OS4zIiBmaWxsPSIjRkZGRkZGIiByPSIyIi8+PGNpcmNsZSBjeD0iNTAiIGN5PSIxOS4zIiBmaWxsPSIjRkZGRkZGIiByPSIyLjEiLz48Y2lyY2xlIGN4PSI3MCIgY3k9IjI5LjMiIGZpbGw9IiNGRkZGRkYiIHI9IjEiLz48Y2lyY2xlIGN4PSIxMTEuOSIgY3k9Ijc4LjYiIGZpbGw9IiNGRkZGRkYiIHI9IjEuNSIvPjxjaXJjbGUgY3g9Ijg0LjQiIGN5PSI4Ny43IiBmaWxsPSIjRkZGRkZGIiByPSIxLjIiLz48Y2lyY2xlIGN4PSI3NC40IiBjeT0iMTE3LjciIGZpbGw9IiNGRkZGRkYiIHI9IjEiLz48Y2lyY2xlIGN4PSI3Ni44IiBjeT0iNzIuOSIgZmlsbD0iI0ZGRkZGRiIgcj0iMS4xIi8+PGNpcmNsZSBjeD0iMzkuOSIgY3k9IjY4LjgiIGZpbGw9IiNGRkZGRkYiIHI9IjEuMiIvPjxjaXJjbGUgY3g9IjE2LjEiIGN5PSI4MC41IiBmaWxsPSIjRkZGRkZGIiByPSIxLjUiLz48L2c+PGc+PGc+PHBhdGggZD0iTTYxLjcsMTI1LjljMC44LDAsMS41LDAuMSwyLjMsMC4xYzAuOCwwLDEuNSwwLDIuMy0wLjFjMi4zLTYuMiwzLjUtMTIuOCwzLjUtMTkuNEg1OC4yICAgICBDNTguMiwxMTMuMiw1OS40LDExOS43LDYxLjcsMTI1Ljl6IiBmaWxsPSIjRkVFMDYxIi8+PHBhdGggZD0iTTY0LjQsMTIyLjNsLTAuNCwwLjhsLTAuNC0wLjhjLTIuMy00LjktMi42LTEwLjMtMi42LTE1LjdsLTAuOCwwSDY3djAgICAgIEM2NywxMTIsNjYuNywxMTcuMyw2NC40LDEyMi4zeiIgZmlsbD0iI0ZGRkZGRiIgb3BhY2l0eT0iMC44Ii8+PC9nPjxnPjxnPjxnPjxwYXRoIGQ9Ik01OCwxMDYuNmgxMmMwLjYtMSwxLjMtMiwxLjgtMy4xSDU2LjJDNTYuNywxMDQuNiw1Ny40LDEwNS42LDU4LDEwNi42eiIgZmlsbD0iIzNEM0UzRCIvPjwvZz48Zz48cGF0aCBkPSJNNzAsMTA3LjFINThjLTAuMiwwLTAuMy0wLjEtMC40LTAuMmMtMC42LTAuOS0xLjItMS45LTEuOS0zLjJjLTAuMS0wLjMsMC0wLjYsMC4yLTAuNyAgICAgICBjMC4zLTAuMSwwLjYsMCwwLjcsMC4yYzAuNiwxLjEsMS4xLDIuMSwxLjcsMi45aDExLjRjMC41LTAuOCwxLjEtMS43LDEuNy0yLjljMC4xLTAuMywwLjQtMC4zLDAuNy0wLjIgICAgICAgYzAuMywwLjEsMC4zLDAuNCwwLjIsMC43Yy0wLjcsMS4zLTEuMywyLjMtMS45LDMuMkM3MC4zLDEwNyw3MC4yLDEwNy4xLDcwLDEwNy4xeiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48ZyBvcGFjaXR5PSIwLjEiPjxwYXRoIGQ9Ik03OCw4NS4xYy0yLDYuNC0xLDEyLjYtNC44LDE4LjNsMSwyLjdoMC40bDAuOC0xLjJjMy45LTUuOSwzLjEtMTIuMiw1LjEtMTguOSAgICAgICBDNzkuOCw4NS41LDc4LjksODUuMSw3OCw4NS4xeiIgZmlsbD0iIzA2MDgwOCIvPjwvZz48Zz48cGF0aCBkPSIgICAgICAgTTUwLjEsODUuMSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjMUUxRTFFIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgc3Ryb2tlLXdpZHRoPSIxLjAzNDIiLz48L2c+PGc+PHBhdGggZD0iTTUyLjcsMzYuMmgyMi42Yy0zLjUtOC4xLTQuNy0xNS43LTExLjMtMjIuM2wwLDBsMCwwQzU3LjQsMjAuNSw1Ni4yLDI4LjEsNTIuNywzNi4yeiIgZmlsbD0iI0ZFRTA2MSIvPjwvZz48Zz48cGF0aCBkPSJNNjQsMTMuOUw2NCwxMy45Yy0xOC4yLDE4LjItMjMuNCw2OC05LjEsODkuNmwwLDBjLTEzLjctMjAuNy02LjItNjkuOSwxMC43LTg3LjkgICAgICAgQzY1LjEsMTUsNjQuNSwxNC40LDY0LDEzLjlMNjQsMTMuOXoiIGZpbGw9IiNGRkZGRkYiLz48L2c+PGc+PHBhdGggZD0iTTc1LjMsMzYuN0g1Mi43Yy0wLjMsMC0wLjUtMC4yLTAuNS0wLjVjMC0wLjMsMC4yLTAuNSwwLjUtMC41aDIyLjZjMC4zLDAsMC41LDAuMiwwLjUsMC41ICAgICAgIEM3NS44LDM2LjQsNzUuNiwzNi43LDc1LjMsMzYuN3oiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PGNpcmNsZSBjeD0iNjQiIGN5PSI1NC45IiBmaWxsPSIjQzBFNUUzIiByPSI4Ii8+PC9nPjxnPjxwYXRoIGQ9Ik02NC45LDQ3LjhjMiwwLDMuOCwwLjcsNS4yLDEuOWMtMS41LTEuNy0zLjctMi44LTYuMS0yLjhjLTQuNCwwLTgsMy42LTgsOGMwLDIuNSwxLjEsNC42LDIuOCw2LjEgICAgICAgYy0xLjItMS40LTEuOS0zLjItMS45LTUuMkM1Ni45LDUxLjQsNjAuNSw0Ny44LDY0LjksNDcuOHoiIGZpbGw9IiNGRkZGRkYiLz48L2c+PGc+PHBhdGggZD0iTTY0LDYzLjRjLTQuNywwLTguNS0zLjgtOC41LTguNWMwLTQuNywzLjgtOC41LDguNS04LjVjNC43LDAsOC41LDMuOCw4LjUsOC41ICAgICAgIEM3Mi41LDU5LjYsNjguNyw2My40LDY0LDYzLjR6IE02NCw0Ny40Yy00LjEsMC03LjUsMy40LTcuNSw3LjVzMy40LDcuNSw3LjUsNy41czcuNS0zLjQsNy41LTcuNVM2OC4xLDQ3LjQsNjQsNDcuNHoiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTczLjEsMTA0SDU0LjljLTAuMiwwLTAuMy0wLjEtMC40LTAuMmMtNi43LTEwLjItOS42LTI3LjUtNy43LTQ2LjRjMS45LTE4LjgsOC4yLTM1LjIsMTYuOS00My45ICAgICAgIGMwLjItMC4yLDAuNS0wLjIsMC43LDBjOC43LDguNywxNSwyNSwxNi45LDQzLjljMS45LDE4LjgtMC45LDM2LjItNy43LDQ2LjRDNzMuNSwxMDMuOSw3My4zLDEwNCw3My4xLDEwNHogTTU1LjEsMTAyLjloMTcuNyAgICAgICBjNi41LTEwLDkuMy0yNyw3LjQtNDUuNUM3OC40LDM5LjIsNzIuMywyMy4yLDY0LDE0LjZjLTguMyw4LjYtMTQuNCwyNC41LTE2LjIsNDIuOUM0NS45LDc1LjksNDguNiw5Mi45LDU1LjEsMTAyLjl6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxwYXRoIGQ9Ik02NCwxMy45TDY0LDEzLjlMNjQsMTMuOWMtMTguMiwxOC4yLTIzLjQsNjgtOS4xLDg5LjZoMTguM0M4Ny40LDgxLjksODIuMiwzMi4xLDY0LDEzLjl6IiBmaWxsPSIjRDhEN0Q4Ii8+PC9nPjxnPjxwYXRoIGQ9Ik01NC45LDEwMy41aDE4LjNjMy41LTUuMyw1LjktMTIuNCw3LjEtMjAuM0g0Ny44QzQ5LDkxLDUxLjMsOTguMSw1NC45LDEwMy41eiIgZmlsbD0iIzlGOUY5RiIvPjwvZz48ZyBvcGFjaXR5PSIwLjEiPjxwb2x5Z29uIGZpbGw9IiMwNjA4MDgiIHBvaW50cz0iNjUuMSwxMDMuNSA2NS4xLDg3LjUgNjkuMSwxMDMuNSAgICAgICIvPjwvZz48Zz48cGF0aCBkPSJNNTEuNiwzNi4yaDI0LjdjLTIuOS05LjEtNy4xLTE3LTEyLjQtMjIuM2wwLDBsMCwwQzU4LjcsMTkuMiw1NC42LDI3LjEsNTEuNiwzNi4yeiIgZmlsbD0iIzMyQTBENyIvPjwvZz48Zz48cGF0aCBkPSJNNjQsMTMuOUw2NCwxMy45Yy0xOC4yLDE4LjItMjMuNCw2OC05LjEsODkuNmwwLDBjLTktMjkuMS02LjUtNzAuOCw5LjctODlDNjQsMTMuOSw2NC41LDE0LjQsNjQsMTMuOSAgICAgICBMNjQsMTMuOXoiIGZpbGw9IiNGRkZGRkYiLz48L2c+PGc+PHBhdGggZD0iTTYxLjMsODEuM2MtMC40LDAtMC44LTAuNC0wLjgtMC44YzAtMC40LDAuNC0wLjgsMC44LTAuOGMwLjQsMCwwLjgsMC40LDAuOCwwLjggICAgICAgQzYyLjEsODAuOSw2MS44LDgxLjMsNjEuMyw4MS4zeiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48Zz48cGF0aCBkPSJNNTUuNiw4MS4zYy0wLjQsMC0wLjgtMC40LTAuOC0wLjhjMC0wLjQsMC40LTAuOCwwLjgtMC44YzAuNCwwLDAuOCwwLjQsMC44LDAuOCAgICAgICBDNTYuNCw4MC45LDU2LjEsODEuMyw1NS42LDgxLjN6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxwYXRoIGQ9Ik00OS45LDgxLjNjLTAuNCwwLTAuOC0wLjQtMC44LTAuOGMwLTAuNCwwLjQtMC44LDAuOC0wLjhjMC40LDAsMC44LDAuNCwwLjgsMC44ICAgICAgIEM1MC43LDgwLjksNTAuNCw4MS4zLDQ5LjksODEuM3oiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTc4LjQsODEuM2MtMC40LDAtMC44LTAuNC0wLjgtMC44YzAtMC40LDAuNC0wLjgsMC44LTAuOHMwLjgsMC40LDAuOCwwLjggICAgICAgQzc5LjIsODAuOSw3OC45LDgxLjMsNzguNCw4MS4zeiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48Zz48cGF0aCBkPSJNNzIuNyw4MS4zYy0wLjQsMC0wLjgtMC40LTAuOC0wLjhjMC0wLjQsMC40LTAuOCwwLjgtMC44YzAuNCwwLDAuOCwwLjQsMC44LDAuOCAgICAgICBDNzMuNSw4MC45LDczLjIsODEuMyw3Mi43LDgxLjN6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxwYXRoIGQ9Ik02Nyw4MS4zYy0wLjQsMC0wLjgtMC40LTAuOC0wLjhjMC0wLjQsMC40LTAuOCwwLjgtMC44YzAuNCwwLDAuOCwwLjQsMC44LDAuOCAgICAgICBDNjcuOCw4MC45LDY3LjUsODEuMyw2Nyw4MS4zeiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48Zz48Zz48cGF0aCBkPSJNNTguMywzOS40Yy0wLjQsMC0wLjgtMC40LTAuOC0wLjhzMC40LTAuOCwwLjgtMC44YzAuNCwwLDAuOCwwLjQsMC44LDAuOFM1OC43LDM5LjQsNTguMywzOS40eiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48Zz48cGF0aCBkPSJNNTMuNCwzOS40Yy0wLjQsMC0wLjgtMC40LTAuOC0wLjhzMC40LTAuOCwwLjgtMC44czAuOCwwLjQsMC44LDAuOFM1My45LDM5LjQsNTMuNCwzOS40eiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48Zz48cGF0aCBkPSJNNzQuNiwzOS40Yy0wLjQsMC0wLjgtMC40LTAuOC0wLjhzMC40LTAuOCwwLjgtMC44czAuOCwwLjQsMC44LDAuOFM3NSwzOS40LDc0LjYsMzkuNHoiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTY5LjcsMzkuNGMtMC40LDAtMC44LTAuNC0wLjgtMC44czAuNC0wLjgsMC44LTAuOGMwLjQsMCwwLjgsMC40LDAuOCwwLjhTNzAuMSwzOS40LDY5LjcsMzkuNHoiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTY0LDM5LjRjLTAuNCwwLTAuOC0wLjQtMC44LTAuOHMwLjQtMC44LDAuOC0wLjhjMC40LDAsMC44LDAuNCwwLjgsMC44UzY0LjQsMzkuNCw2NCwzOS40eiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48L2c+PGc+PHBhdGggZD0iTTczLjIsMTAzLjVsNi4xLDguOWMwLjIsMC41LDAuNywwLjksMS4yLDAuOWg0LjFjMC43LDAsMS4zLTAuNiwxLjMtMS4zTDgyLDg5LjJsLTIuNC0yLjQgICAgICAgQzc5LjYsODYuOCw3Ni45LDk3LjcsNzMuMiwxMDMuNXoiIGZpbGw9IiNDN0IxM0MiLz48L2c+PGcgb3BhY2l0eT0iMC4xIj48cGF0aCBkPSJNODIsODkuMmwtMi40LTIuNGMwLDAtMi43LDEwLjktNi41LDE2LjZsMiwyLjlsMC43LTEuMWMyLjctNC4xLDQuOC05LjMsNi4zLTE1LjNMODIsODkuMnoiIGZpbGw9IiMwNjA4MDgiLz48L2c+PGc+PHBhdGggZD0iTTg0LjUsMTEzLjdoLTQuMWMtMC43LDAtMS40LTAuNC0xLjctMS4xbC02LTguOGMtMC4yLTAuMi0wLjEtMC42LDAuMS0wLjdjMC4yLTAuMiwwLjYtMC4xLDAuNywwLjEgICAgICAgbDYuMSw4LjljMCwwLDAsMC4xLDAuMSwwLjFjMC4xLDAuMywwLjQsMC41LDAuNywwLjVoNC4xYzAuNCwwLDAuOC0wLjMsMC44LTAuN2wtMy44LTIyLjVsLTIuMy0yLjNjLTAuMi0wLjItMC4yLTAuNSwwLTAuNyAgICAgICBjMC4yLTAuMiwwLjUtMC4yLDAuNywwbDIuNCwyLjRjMC4xLDAuMSwwLjEsMC4yLDAuMSwwLjNsMy44LDIyLjdjMCwwLDAsMC4xLDAsMC4xQzg2LjMsMTEyLjksODUuNSwxMTMuNyw4NC41LDExMy43eiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48Zz48cGF0aCBkPSJNNTQuOSwxMDMuNWwtNi4xLDguOWMtMC4yLDAuNS0wLjcsMC45LTEuMiwwLjloLTQuMWMtMC43LDAtMS4zLTAuNi0xLjMtMS4zTDQ2LDg5LjJsMi4zLTIuMyAgICAgICBDNDguMyw4Ni45LDUxLjEsOTcuNyw1NC45LDEwMy41eiIgZmlsbD0iI0M3QjEzQyIvPjwvZz48Zz48cGF0aCBkPSJNNDYsODkuMmwtMy44LDIyLjdjMCwwLjctMC4xLDEuMywwLjYsMS4zbDUuNS0yNi4zQzQ4LjIsODYuNSw0Niw4OS4yLDQ2LDg5LjJ6IiBmaWxsPSIjRkZGRkZGIi8+PC9nPjxnIG9wYWNpdHk9IjAuMSI+PHBhdGggZD0iTTQ4LjMsODYuOUw0Niw4OS4ybC0wLjIsMC45YzEuNSw1LjksMy42LDExLjEsNi4zLDE1LjFsMC43LDEuMWwyLTIuOUM1MS4xLDk3LjcsNDguMyw4Ni45LDQ4LjMsODYuOXoiIGZpbGw9IiMwNjA4MDgiLz48L2c+PGc+PHBhdGggZD0iTTQ3LjYsMTEzLjdoLTQuMWMtMSwwLTEuOC0wLjgtMS44LTEuOGMwLDAsMC0wLjEsMC0wLjFsMy44LTIyLjdjMC0wLjEsMC4xLTAuMiwwLjEtMC4zbDIuMy0yLjQgICAgICAgYzAuMi0wLjIsMC41LTAuMiwwLjcsMGMwLjIsMC4yLDAuMiwwLjUsMCwwLjdsLTIuMiwyLjNsLTMuOCwyMi41YzAsMC40LDAuNCwwLjcsMC44LDAuN2g0LjFjMC4zLDAsMC42LTAuMiwwLjctMC41ICAgICAgIGMwLDAsMC0wLjEsMC4xLTAuMWw2LjEtOC45YzAuMi0wLjIsMC41LTAuMywwLjctMC4xYzAuMiwwLjIsMC4zLDAuNSwwLjEsMC43bC02LDguOEM0OSwxMTMuMiw0OC4zLDExMy43LDQ3LjYsMTEzLjd6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxwYXRoIGQ9Ik03Ni4xLDM2LjdINTEuOWMtMC4zLDAtMC41LTAuMi0wLjUtMC41YzAtMC4zLDAuMi0wLjUsMC41LTAuNWgyNC4xYzAuMywwLDAuNSwwLjIsMC41LDAuNSAgICAgICBDNzYuNiwzNi40LDc2LjMsMzYuNyw3Ni4xLDM2Ljd6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxjaXJjbGUgY3g9IjY0IiBjeT0iNTQuOSIgZmlsbD0iIzNEM0UzRCIgcj0iOCIvPjwvZz48Zz48Zz48cGF0aCBkPSJNNjUuNyw0OC41YzEuOCwwLDMuNCwwLjYsNC44LDEuNmMtMS41LTItMy44LTMuMi02LjQtMy4yYy00LjQsMC04LDMuNi04LDhjMCwyLjYsMS4zLDUsMy4yLDYuNCAgICAgICAgYy0xLTEuMy0xLjYtMy0xLjYtNC44QzU3LjYsNTIuMSw2MS4yLDQ4LjUsNjUuNyw0OC41eiIgZmlsbD0iI0ZGRkZGRiIvPjwvZz48L2c+PGc+PHBhdGggZD0iTTY0LDYzLjRjLTQuNywwLTguNS0zLjgtOC41LTguNWMwLTQuNywzLjgtOC41LDguNS04LjVjNC43LDAsOC41LDMuOCw4LjUsOC41ICAgICAgIEM3Mi41LDU5LjYsNjguNyw2My40LDY0LDYzLjR6IE02NCw0Ny40Yy00LjEsMC03LjUsMy40LTcuNSw3LjVzMy40LDcuNSw3LjUsNy41czcuNS0zLjQsNy41LTcuNVM2OC4xLDQ3LjQsNjQsNDcuNHoiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTczLjEsMTA0SDU0LjljLTAuMiwwLTAuMy0wLjEtMC40LTAuMmMtNi43LTEwLjItOS42LTI3LjUtNy43LTQ2LjRjMS45LTE4LjgsOC4yLTM1LjIsMTYuOS00My45ICAgICAgIGMwLjItMC4yLDAuNS0wLjIsMC43LDBjOC43LDguNywxNSwyNSwxNi45LDQzLjljMS45LDE4LjgtMC45LDM2LjItNy43LDQ2LjRDNzMuNSwxMDMuOSw3My4zLDEwNCw3My4xLDEwNHogTTU1LjEsMTAyLjloMTcuNyAgICAgICBjNi41LTEwLDkuMy0yNyw3LjQtNDUuNUM3OC40LDM5LjIsNzIuMywyMy4yLDY0LDE0LjZjLTguMyw4LjYtMTQuNCwyNC41LTE2LjIsNDIuOUM0NS45LDc1LjksNDguNiw5Mi45LDU1LjEsMTAyLjl6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxwYXRoIGQ9Ik02OS40LDQ2LjNjLTAuNCwwLTAuOC0wLjQtMC44LTAuOGMwLTAuNCwwLjQtMC44LDAuOC0wLjhjMC40LDAsMC44LDAuNCwwLjgsMC44ICAgICAgIEM3MC4yLDQ2LDY5LjgsNDYuMyw2OS40LDQ2LjN6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxwYXRoIGQ9Ik03My4zLDUwLjJjLTAuNCwwLTAuOC0wLjQtMC44LTAuOGMwLTAuNCwwLjQtMC44LDAuOC0wLjhzMC44LDAuNCwwLjgsMC44ICAgICAgIEM3NC4xLDQ5LjgsNzMuNyw1MC4yLDczLjMsNTAuMnoiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTc0LjgsNTUuN2MtMC40LDAtMC44LTAuNC0wLjgtMC44YzAtMC40LDAuNC0wLjgsMC44LTAuOHMwLjgsMC40LDAuOCwwLjggICAgICAgQzc1LjYsNTUuMyw3NS4yLDU1LjcsNzQuOCw1NS43eiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48Zz48cGF0aCBkPSJNNzMuMyw2MS4xYy0wLjQsMC0wLjgtMC40LTAuOC0wLjhjMC0wLjQsMC40LTAuOCwwLjgtMC44YzAuNCwwLDAuOCwwLjQsMC44LDAuOCAgICAgICBDNzQuMSw2MC43LDczLjcsNjEuMSw3My4zLDYxLjF6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxwYXRoIGQ9Ik02OS40LDY1Yy0wLjQsMC0wLjgtMC40LTAuOC0wLjhjMC0wLjQsMC40LTAuOCwwLjgtMC44czAuOCwwLjQsMC44LDAuOEM3MC4yLDY0LjYsNjkuOSw2NSw2OS40LDY1eiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48Zz48cGF0aCBkPSJNNjQsNjYuNGMtMC40LDAtMC44LTAuNC0wLjgtMC44czAuNC0wLjgsMC44LTAuOGMwLjQsMCwwLjgsMC40LDAuOCwwLjhTNjQuNCw2Ni40LDY0LDY2LjR6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxwYXRoIGQ9Ik01OC42LDY1Yy0wLjQsMC0wLjgtMC40LTAuOC0wLjhjMC0wLjQsMC40LTAuOCwwLjgtMC44YzAuNCwwLDAuOCwwLjQsMC44LDAuOEM1OS4zLDY0LjYsNTksNjUsNTguNiw2NSAgICAgICB6IiBmaWxsPSIjMUUxRTFFIi8+PC9nPjxnPjxwYXRoIGQ9Ik01NC43LDYxLjFjLTAuNCwwLTAuOC0wLjQtMC44LTAuOGMwLTAuNCwwLjQtMC44LDAuOC0wLjhjMC40LDAsMC44LDAuNCwwLjgsMC44ICAgICAgIEM1NS41LDYwLjcsNTUuMSw2MS4xLDU0LjcsNjEuMXoiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTUzLjIsNTUuNmMtMC40LDAtMC44LTAuNC0wLjgtMC44czAuNC0wLjgsMC44LTAuOGMwLjQsMCwwLjgsMC40LDAuOCwwLjhTNTMuNyw1NS42LDUzLjIsNTUuNnoiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTU0LjcsNTAuMmMtMC40LDAtMC44LTAuNC0wLjgtMC44YzAtMC40LDAuNC0wLjgsMC44LTAuOGMwLjQsMCwwLjgsMC40LDAuOCwwLjggICAgICAgQzU1LjUsNDkuOSw1NS4xLDUwLjIsNTQuNyw1MC4yeiIgZmlsbD0iIzFFMUUxRSIvPjwvZz48Zz48cGF0aCBkPSJNNTguNiw0Ni4zYy0wLjQsMC0wLjgtMC40LTAuOC0wLjhjMC0wLjQsMC40LTAuOCwwLjgtMC44YzAuNCwwLDAuOCwwLjQsMC44LDAuOCAgICAgICBDNTkuMyw0Niw1OSw0Ni4zLDU4LjYsNDYuM3oiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTY0LDQ0LjljLTAuNCwwLTAuOC0wLjQtMC44LTAuOGMwLTAuNCwwLjQtMC44LDAuOC0wLjhzMC44LDAuNCwwLjgsMC44QzY0LjgsNDQuNSw2NC40LDQ0LjksNjQsNDQuOXoiIGZpbGw9IiMxRTFFMUUiLz48L2c+PGc+PHBhdGggZD0iTTc5LjksODMuN0g0OC4zYy0wLjMsMC0wLjUtMC4yLTAuNS0wLjVjMC0wLjMsMC4yLTAuNSwwLjUtMC41aDMxLjVjMC4zLDAsMC41LDAuMiwwLjUsMC41ICAgICAgIEM4MC40LDgzLjQsODAuMiw4My43LDc5LjksODMuN3oiIGZpbGw9IiMxRTFFMUUiLz48L2c+PC9nPjxnPjxwYXRoIGQ9Ik02NCwxMTMuMkw2NCwxMTMuMmMwLjYsMCwxLjEtMC41LDEuMS0xLjFWODcuNWMwLTAuNi0wLjUtMS4xLTEuMS0xLjFsMCwwYy0wLjYsMC0xLjEsMC41LTEuMSwxLjF2MjQuNiAgICAgIEM2Mi45LDExMi43LDYzLjQsMTEzLjIsNjQsMTEzLjJ6IiBmaWxsPSIjQzdCMTNDIi8+PC9nPjxnPjxwYXRoIGQ9Ik02My40LDExMi43bDEtMjUuOGMwLTAuNS0wLjQtMC4yLDAtMC40Yy0wLjEsMC0wLjItMC4xLTAuNC0wLjFjLTAuNiwwLTEuMSwwLjUtMS4xLDEuMXYyNC42ICAgICAgYzAsMC42LDAuNSwxLjEsMS4xLDEuMWMwLjEsMC0wLjEsMCwwLDBDNjMuNiwxMTMsNjMuNCwxMTMuMiw2My40LDExMi43eiIgZmlsbD0iI0ZGRkZGRiIvPjwvZz48Zz48cGF0aCBkPSJNNjQsMTEzLjdjLTAuOSwwLTEuNi0wLjctMS42LTEuNlY4Ny41YzAtMC45LDAuNy0xLjYsMS42LTEuNmMwLjksMCwxLjYsMC43LDEuNiwxLjZ2MjQuNiAgICAgIEM2NS42LDExMyw2NC45LDExMy43LDY0LDExMy43eiBNNjQsODYuOWMtMC4zLDAtMC42LDAuMy0wLjYsMC42djI0LjZjMCwwLjMsMC4zLDAuNiwwLjYsMC42YzAuMywwLDAuNi0wLjMsMC42LTAuNlY4Ny41ICAgICAgQzY0LjYsODcuMiw2NC4zLDg2LjksNjQsODYuOXoiIGZpbGw9IiMxRTFFMUUiLz48L2c+PC9nPjwvZz48L2c+PC9zdmc+" style="width: 40%; margin-bottom: 40px; background: rgba(255, 255, 255, 0.9); padding: 1px; border-radius: 50%;">
			</div>
			<header class="widget__header one-btn">
				<div class="widget__title">
					<div class="main-logo"><img src="img/logo.png"></div> Sign in
				</div>
				<div class="widget__config">
					<a href="#" onclick="window.location.href = 'index.php'"><i class="pe-7s-help1"></i></a>
				</div>
			</header>
	
			<div class="widget__content">
				<input type="text" placeholder="Username" class="user-name">
				<input type="password" placeholder="Password" class="user-pass">
				<button class="sign-in">Sign in</button>
			</div>
	<!--
			<div class="login__remember text-center">
				<input type="checkbox" class="custom-checkbox" id="cc1" checked>
				<label for="cc1"></label>
				Remember me
			</div>
	-->
		</article><!-- /widget -->
	</div>
	</div>
	<script>
		
		particlesJS("particles-js", {
		  "particles": {
		    "number": {
		      "value": 100,
		      "density": {
		        "enable": true,
		        "value_area": 800
		      }
		    },
		    "color": {
		      "value": "#ffffff"
		    },
		    "shape": {
		      "type": "circle",
		      "stroke": {
		        "width": 0,
		        "color": "#000000"
		      },
		      "polygon": {
		        "nb_sides": 5
		      },
		      "image": {
		        "src": "img/github.svg",
		        "width": 100,
		        "height": 100
		      }
		    },
		    "opacity": {
		      "value": 0.5,
		      "random": false,
		      "anim": {
		        "enable": false,
		        "speed": 1,
		        "opacity_min": 0.1,
		        "sync": false
		      }
		    },
		    "size": {
		      "value": 2,
		      "random": true,
		      "anim": {
		        "enable": false,
		        "speed": 0,
		        "size_min": 0,
		        "sync": false
		      }
		    },
		    "line_linked": {
		      "enable": true,
		      "distance": 150,
		      "color": "#ffffff",
		      "opacity": 0.4,
		      "width": 1
		    },
		    "move": {
		      "enable": false,
		      "speed": 2,
		      "direction": "none",
		      "random": false,
		      "straight": false,
		      "out_mode": "out",
		      "bounce": false,
		      "attract": {
		        "enable": false,
		        "rotateX": 600,
		        "rotateY": 1200
		      }
		    }
		  },
		  "interactivity": {
		    "detect_on": "canvas",
		    "events": {
		      "onhover": {
		        "enable": true,
		        "mode": "bubble"
		      },
		      "onclick": {
		        "enable": false,
		        "mode": "push"
		      },
		      "resize": true
		    },
		    "modes": {
		      "grab": {
		        "distance": 400,
		        "line_linked": {
		          "opacity": 1
		        }
		      },
		      "bubble": {
		        "distance": 267.9854800594439,
		        "size": 4,
		        "duration": 2.1926084732136317,
		        "opacity": 1,
		        "speed": 3
		      },
		      "repulse": {
		        "distance": 200,
		        "duration": 0.4
		      },
		      "push": {
		        "particles_nb": 4
		      },
		      "remove": {
		        "particles_nb": 2
		      }
		    }
		  },
		  "retina_detect": true
		});
		
		$(".sign-in").click(function(){
			if($(".user-name").val() == "jako.fm" && $(".user-pass").val() == "radiojako123")
				window.location.href = 'dashboard.php';
			else {
				$(".mess-error").fadeOut(100);
				$(".mess-error").fadeIn(300);
			}
		});
	</script>
				
</body>
</html>