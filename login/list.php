<?php require_once "inc/inc.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Phalcon Admin :: Statistics</title>
	
	<link rel="icon" sizes="192x192" href="img/touch-icon.png" /> 
	<link rel="apple-touch-icon" href="img/touch-icon-iphone.png" /> 
	<link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
	<link rel="apple-touch-icon" sizes="120x120" href="img/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
	
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
	
</head>
<body class="bg3">
	<div id="loading">
		<div class="loader loader-light loader-large"></div>
	</div>
	
	<?php require_once "inc/header.php"; ?>

	<div class="wrapper">

		<?php require_once "inc/sidebar.php"; ?>
		
		<section class="content">
			<header class="main-header">
				<div class="main-header__nav">
					<h1 class="main-header__title">
						<i class="pe-7f-note2"></i>
						<span>Boost - List</span>
					</h1>
					<ul class="main-header__breadcrumb">
						<li><a href="#" onclick="return false;">Home</a></li>
						<li class="active"><a href="#" onclick="return false;">Boosted - List</a></li>
					</ul>
				</div>
				
			</header> <!-- /main-header -->


				<div class="row">
				
				<div class="alert alert-fixed alert-success alert-dismissible mess-success" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true"><i class="pe-7s-close"></i></span>
					</button>
					<div class="alert__icon pull-left">
						<i class="pe-7s-check"></i>
					</div>
					<p class="alert__text"> Success.. </p>
				</div>
				<br>
				<div class="alert alert-fixed alert-warning alert-dismissible mess-error" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true"><i class="pe-7s-close"></i></span>
					</button>
					<div class="alert__icon pull-left">
						<i class="pe-7s-attention"></i>
					</div>
					<p class="alert__text"> Error!! </p>
				</div>
				<br>

					<div class="tab-radio-full">
						<input type="radio" id="tab_radio_1" name="tab-radio-2" value="tabr1" checked>
						<label for="tab_radio_1" class="btn-md">To do</label>
						<input type="radio" id="tab_radio_2" name="tab-radio-2" value="tabr2" >
						<label for="tab_radio_2" class="btn-md">Published</label>
					</div> <!-- /tab-radio-full (only for full width) -->

					<div data-tab-radio="tab-radio-2" class="tab-radio-content" id="tabr1">
						<!-- tab 1 content -->
						
						<div class="col-md-12">
							<article class="widget">
								<header class="widget__header">
									<div class="widget__title">
										<i class="pe-7s-menu"></i><h3>Scheduled</h3>
									</div>
									<div class="widget__config">
										<a href="#"><i class="pe-7f-refresh"></i></a>
										<a href="#"><i class="pe-7s-close"></i></a>
									</div>
								</header>
								
								<div class="widget__content table-responsive">
									
									<table class="table table-striped table-hover media-table scheduled-table">
								  	<thead>
								  		<tr>
								  			<th width="270">Post Info</th>
								  			<th>Clicks</th>
								  			<th>Description</th>
								  			<th width="120">Scheduled</th>
								  			<!-- <th>Edit</th> -->
								  			<th>Del</th>
								  		</tr>
								  	</thead>
								  	<tbody>
								<?php

									$now = date('Y-m-d H:i:s');

									// select articles
									$data = $db->from('ARTICLES')->where('PUBLISHED', 0)->sortDesc('ADDDATE')->limit(30)->many();

									for ($i = 0; $i < count($data); $i++) {
								?>
								  	
								  		<tr class="spacer"></tr>
								  		<tr>
								  			<td>
								  				<div class="media">
								  						<figure class="pull-left post__img">
															<a href="<?=$data[$i]['LINK']?>" target="_blank">
																<img class="media-object" src="<?=$data[$i]['IMAGE']?>" alt="user">
															</a>
														</figure>
														<div class="media-body post_desc">
															<h3><?=$data[$i]['TITLE']?></h3>
															<p><?=$data[$i]['COMMENT']?></p>
														</div>
													</div>
								  			</td>
								  			<td>
								  				<p class="post__date">324 <i class="pe-7f-mouse"></i></p>
								  			</td>
								  			<td>
								  				<p class="post__info"><?=$data[$i]['DESCRIPTION']?></p>
								  			</td>
								  			<td>
								  				<p class="post__date"><?=$data[$i]['SCHEDULE']?></p>
								  			</td>
								  			<!-- <td>
								  				<i class="pe-7f-edit edit-btn"></i>
								  			</td> -->
								  			<td>
								  				<i class="pe-7f-close del-btn" dataid="<?=$data[$i]['ID']?>"></i>
								  			</td>
								  		</tr>
								  	
								<?php

									}
								?>	

								  	</tbody>
									</table>
									

									
								</div> <!-- /widget__content -->

							</article><!-- /widget -->
						</div>

					</div>
					<div data-tab-radio="tab-radio-2" class="tab-radio-content" id="tabr2">
						<!-- tab 2 content -->
						
						<div class="col-md-12">
							<article class="widget">
								<header class="widget__header">
									<div class="widget__title">
										<i class="pe-7s-menu"></i><h3>Last (30) Boosted News</h3>
									</div>
									<div class="widget__config">
										<a href="#"><i class="pe-7f-refresh"></i></a>
										<a href="#"><i class="pe-7s-close"></i></a>
									</div>
								</header>
								
								<div class="widget__content table-responsive">
									
									<table class="table table-striped table-hover media-table published-table">
								  	<thead>
								  		<tr>
								  			<th width="270">Post Info</th>
								  			<th>Description</th>
								  			<th>Clicks</th>
								  			<th width="120">Published</th>
								  		</tr>
								  	</thead>
								  	<tbody>
								<?php

									$now = date('Y-m-d H:i:s');

									// select articles
									$data = $db->from('ARTICLES')->where('PUBLISHED', 1)->sortDesc('ADDDATE')->limit(30)->many();

									for ($i = 0; $i < count($data); $i++) {
								?>
								  	
								  		<tr class="spacer"></tr>
								  		<tr>
								  			<td>
								  				<div class="media">
													<figure class="pull-left post__img">
														<a href="<?=$data[$i]['LINK']?>" target="_blank">
															<img class="media-object" src="<?=$data[$i]['IMAGE']?>" alt="user">
														</a>
													</figure>
													<div class="media-body post_desc">
														<h3><?=$data[$i]['TITLE']?></h3>
														<p><?=$data[$i]['COMMENT']?></p>
													</div>
												</div>
								  			</td>
								  			<td>
								  				<p class="post__info"><?=$data[$i]['DESCRIPTION']?></p>
								  			</td>
								  			<td>
								  				<p class="post__date">324 <i class="pe-7f-mouse"></i></p>
								  			</td>
								  			<td>
								  				<p class="post__date"><?=$data[$i]['SCHEDULE']?></p>
								  			</td>
								  		</tr>
								  	
								<?php

									}
								?>	

								  	</tbody>
									</table>
									

									
								</div> <!-- /widget__content -->

							</article><!-- /widget -->
						</div>
					</div>


				</div> <!-- /row -->

			<footer class="footer-brand">
				<img src="img/logo_trim.png">
				<p>© 2014 Phalcon. All rights reserved</p>
			</footer>


		</section> <!-- /content -->

	</div>
	
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/app.js"></script>
	<script type="text/javascript" src="js/rome.min.js"></script>
	<script>
		$(document).ready(function(){
			// delete news
			$('.del-btn').click(function(event) {
				event.preventDefault();

			    if(!confirm("It will be removed from Schedule.. Are you sure?"))
			        return false;

				var dataDel = {};

				// organize data
				dataDel['ID'] = $(this).attr('dataid');

				// send data to service
			    $.ajax({
			        url: "exec.php?act=del",
			        type: "post",
			        data: dataDel
			    })
			    .done(function() {
			    	//  success
					/*window.setTimeout(function(){
						$('.mess-success').fadeIn('slow');
					}, 250);*/
					/*window.setTimeout(function(){
						$('.mess-success').fadeOut('slow');
					}, 4500);*/
				})
				.fail(function() {
					//  error
					window.setTimeout(function(){
						$('.mess-error').fadeIn('slow');
					}, 250);
					/*window.setTimeout(function(){
						$('.mess-error').fadeOut('slow');
					}, 4500);*/
				})
				.always(function() {
					// reload page
					window.setTimeout(function(){
						location.reload();
					}, 350);
				});
					
			});
		})
	</script>

</body>
</html>