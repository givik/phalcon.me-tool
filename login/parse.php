<?php

if(!isset($_GET['url']))
	exit();

require_once('inc/OpenGraph.php');
		
$graph = OpenGraph::fetch($_GET['url']);
$metas = array();

foreach ($graph as $key => $value) {
    $metas["$key"] = "$value";
}

echo json_encode($metas);