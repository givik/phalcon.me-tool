<?php require_once "inc/inc.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Phalcon Admin :: Statistics</title>
	
	<link rel="icon" sizes="192x192" href="img/touch-icon.png" /> 
	<link rel="apple-touch-icon" href="img/touch-icon-iphone.png" /> 
	<link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
	<link rel="apple-touch-icon" sizes="120x120" href="img/touch-icon-iphone-retina.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
	
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.min.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">
	
</head>
<body class="bg3">
	<div id="loading">
		<div class="loader loader-light loader-large"></div>
	</div>

	<?php require_once "inc/header.php"; ?>

	<div class="wrapper">

		<?php require_once "inc/sidebar.php"; ?>
		
		<section class="content">
			<header class="main-header">
				<div class="main-header__nav">
					<h1 class="main-header__title">
						<i class="pe-7s-graph1"></i>
						<span>Statistics</span>
					</h1>
					<ul class="main-header__breadcrumb">
						<li><a href="#" onclick="return false;">Home</a></li>
						<li class="active"><a href="#" onclick="return false;">Statistics</a></li>
					</ul>
				</div>
				
			</header> <!-- /main-header -->



				<div class="row">
					<div class="col-md-12">
						<div id="chartdiv" style="width: 100%; height: 362px;"></div>
					</div>
				</div>




				<div class="row">

					<div class="col-md-7">
						<article class="widget">
							<header class="widget__header">
								<div class="widget__title">
									<i class="pe-7f-graph3"></i><h3>Bar Chart</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
									<a href="#"><i class="pe-7s-close"></i></a>
								</div>
							</header>

							<div class="widget__content filled pad20">
								
								<div class="row">
									<div class="col-md-12 text-center btn__showcase2">
										<div id="chartdiv2" style="width: 100%; height: 362px;"></div>
									</div>

								</div>

							</div>
						</article><!-- /widget -->
					</div>
					
					<div class="col-md-5">
						<article class="widget">
							<header class="widget__header">
								<div class="widget__title">
									<i class="pe-7s-graph"></i><h3>Donut Chart</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
									<a href="#"><i class="pe-7s-close"></i></a>
								</div>
							</header>
							
							<div class="widget__content filled widget-ui">
								
								<div class="row">
									<div class="col-md-12 text-center">
										<div id="chartdiv3" style="width: 100%; height: 362px;"></div>
									</div>
								</div>
								
								
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

				</div> <!-- /row -->


				<div class="row">
					
					<div class="col-md-6">
						<article class="widget">
							<header class="widget__header">
								<div class="widget__title">
									<i class="pe-7s-graph1"></i><h3>Bubble Chart</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
									<a href="#"><i class="pe-7s-close"></i></a>
								</div>
							</header>
							
							<div class="widget__content filled widget-ui">
								
								<div class="row">
									<div class="col-md-12 text-center">
										<div id="chartdiv4" style="width: 100%; height: 362px;"></div>
									</div>
								</div>
								
								
							</div> <!-- /widget__content -->

						</article><!-- /widget -->
					</div>

					<div class="col-md-6">
						<article class="widget">
							<header class="widget__header">
								<div class="widget__title">
									<i class="pe-7s-graph3"></i><h3>Polar Chart</h3>
								</div>
								<div class="widget__config">
									<a href="#"><i class="pe-7f-refresh"></i></a>
									<a href="#"><i class="pe-7s-close"></i></a>
								</div>
							</header>

							<div class="widget__content filled pad20">
								
								<div class="row">
									<div class="col-md-12 text-center btn__showcase2">
										<div id="chartdiv5" style="width: 100%; height: 362px;"></div>
									</div>

								</div>

							</div>
						</article><!-- /widget -->
					</div>


				</div> <!-- /row -->

				


			<footer class="footer-brand">
				<img src="img/logo_trim.png">
				<p>© 2014 Phalcon. All rights reserved</p>
			</footer>


		</section> <!-- /content -->

	</div>


	
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/amcharts/amcharts.js"></script>
	<script type="text/javascript" src="js/amcharts/serial.js"></script>
	<script type="text/javascript" src="js/amcharts/pie.js"></script>
	<script type="text/javascript" src="js/amcharts/xy.js"></script>
	<script type="text/javascript" src="js/amcharts/radar.js"></script>
	<script type="text/javascript" src="js/charts.js"></script>
</body>
</html>