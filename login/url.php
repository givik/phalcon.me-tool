<?php
	
	if(isset($_GET['id']) && $_GET['id']) {
		
		require_once __DIR__ . "/inc/inc.php";
		require_once __DIR__ . "/inc/Mobile_Detect.php";
		
		$article = $db->from('ARTICLES')->where('ID', $_GET['id'])->one();
		
		if(!count($article)) {
			header('Location: http://phalcon.me/');
			exit();
		}
		
		function getDevice() {
			$detect = new Mobile_Detect;
			
			// DETECT DEVICE
			$device = 1;
			
			if( !$detect->isMobile() ) {
				// PC
				$device = 1;
			}
			else if( $detect->isTablet() ){
				// Tablet
				$device = 2;
			}
			else if( $detect->isMobile() && !$detect->isTablet() ){
				// Phone
				$device = 3;
			}
			
			return $device;
		}
		
		function getLocation() {
			$ip = $_SERVER['REMOTE_ADDR'];
			$loc = (array) json_decode(file_get_contents("http://freegeoip.net/json/{$ip}"));
			return $loc;
		}
		
		// Statistics Array
		$STATISTICS['ARTICLEID'] = $article['ID'];
		$STATISTICS['DEVICE'] = getDevice();
		$STATISTICS['IP'] = $_SERVER['REMOTE_ADDR'];
		$STATISTICS['REGION'] = getLocation()['city'];
		$STATISTICS['DATETIME'] = date("Y-m-d H:i:s");
		
		
		$res = $db->from('STATISTICS')->insert($STATISTICS)->execute();
		
		header('Location: ' . $article['LINK']);
	}
	else {
		header('Location: http://phalcon.me/');
	}
	
?>